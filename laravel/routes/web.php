<?php
Route::get('/pay','PaymentController@getPay')->name('get-pay');

Route::post('/paydemo','PaymentController@getPay')->name('pay');

Route::post('/placeorder','PaymentController@getPay')->name('placeorder');

Route::post('/paystatus','PaymentController@orderStatus')->name('orderStatus');

Route::resource('payment', 'PaymentController');

Route::get('/booked/order/{id}/{status?}', 'PaymentController@previous')->name('previous');