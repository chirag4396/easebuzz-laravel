@extends('user.layouts.master')

@section('content')
<div id="mail" class="banner-bottom mail contact" style="padding-top: 5em;">
	<div class="container">
		<div class="title" style="padding-bottom: 0px;">
			<h2>Pay</h2>
		</div>
		<div class="w3ls_banner_bottom_grids">
			<div class="col-md-6 col-md-offset-3 w3layouts_mail_grid_left">
				<div class="agileits_mail_grid_left">
					@if ($status)
					<div class="alert">								 
						<strong>Thank you for choosing {{ config('app.name') }}</strong>
						<p>We recieved your payment successfully, for any query please contact us at {{ config('app.mobile') }} or {{ config('app.support_email') }}</p>
					</div>
					@else
					<div class="alert fail">
						<strong>Payment Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.mobile') }}</b> or <b>{{ config('app.support_email') }}</b>
					</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
@endsection