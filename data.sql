-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 24, 2018 at 06:11 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `my_fab_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `order_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_d_id` varchar(25) NOT NULL,
  `order_user_id` int(10) NOT NULL,
  `order_package` int(10) NOT NULL,
  `order_total` decimal(10,2) DEFAULT '0.00',
  `order_status` int(10) NOT NULL,
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
CREATE TABLE IF NOT EXISTS `order_status` (
  `os_id` int(10) NOT NULL AUTO_INCREMENT,
  `os_title` varchar(100) NOT NULL,
  PRIMARY KEY (`os_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `ph_id` int(10) NOT NULL AUTO_INCREMENT,
  `ph_order_id` int(10) NOT NULL,
  `ph_status` varchar(50) NOT NULL,
  `ph_txn_id` varchar(100) NOT NULL,
  `ph_amount` float NOT NULL,
  `ph_fullname` varchar(150) NOT NULL,
  `ph_email` varchar(150) NOT NULL,
  `ph_phone` bigint(12) NOT NULL,
  `ph_bank_ref_num` varchar(50) NOT NULL,
  `ph_bankcode` varchar(20) NOT NULL,
  `ph_unmapped` varchar(150) NOT NULL,
  `ph_added_on` datetime NOT NULL,
  `ph_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ph_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
COMMIT;
